/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

/**
 * Modelamiento de una matriz usando el concepto de vector de vectores
 *
 * @author madarme
 */
public class Matriz_Numeros {

    private ListaNumeros[] filas;

    public Matriz_Numeros() {
    }

    /**
     * Puedo utilizarlo para matrices dispersa
     *
     * @param cantFilas
     */
    public Matriz_Numeros(int cantFilas) {
        if (cantFilas <= 0) {
            throw new RuntimeException("No se puede crear la matriz , sus cantidad de filas debe ser mayor a 0");
        }
        this.filas = new ListaNumeros[cantFilas];
    }

    /**
     * Creación de matrices cuadradas o bien rectangulares
     *
     * @param cantFilas
     * @param cantColumnas
     */
    public Matriz_Numeros(int cantFilas, int cantColumnas) {
        if (cantFilas <= 0 || cantColumnas <= 0) {
            throw new RuntimeException("No se puede crear la matriz , sus cantidad de filas o columnas debe ser mayor a 0");
        }
        this.filas = new ListaNumeros[cantFilas];
        //Creando columnas:
        for (int i = 0; i < cantFilas; i++) {
            this.filas[i] = new ListaNumeros(cantColumnas);
        }

    }

    public ListaNumeros[] getFilas() {
        return filas;
    }

    public void setFilas(ListaNumeros[] filas) {
        this.filas = filas;
    }

    private void validar(int i) {
        if (i < 0 || i >= this.filas.length) {
            throw new RuntimeException("Índice fuera de rango para una fila:" + i);
        }
    }

    public void adicionarVector(int i, ListaNumeros listaNueva) {
        this.validar(i);
        this.filas[i] = listaNueva;
    }

    /**
     * Esté mètodo funciona SI Y SOLO SI LA MATRIZ ESTÁ CREADA CON FILAS Y
     * COLUMNAS
     *
     * @param i índice de la fila
     * @param j índice de la columna
     * @param nuevoDato dato a ingresar en i,j
     * @throws java.lang.Exception
     */
    public void setElemento(int i, int j, float nuevoDato) throws Exception {
        if ((i+1 > this.filas.length || i < 0) || (j+1 > this.filas[i].length() || j < 0)) {
            throw new Exception("No se puede enviar a la posición deseada.\n"
                    + (((i+1 > this.filas.length || i < 0) && (j+1 > this.filas[0].length() || j < 0))
                    ? "Posición de fila y columna no es válida" : ((i+1 > this.filas.length || i < 0)
                            ? "Posición de fila no válida" : "Posición de columna no válida")));
        } else {

            ListaNumeros p = filas[i];
            p.adicionar(j, nuevoDato);
            adicionarVector(i, p);
        }
    }

    @Override
    public String toString() {
        String msg = "";

        for (ListaNumeros myLista : this.filas) {
            msg += myLista.toString() + "\n";
        }
        return msg;
    }

    public int length_filas() {
        return this.filas.length;
    }

    /**
     * Obtiene que tipo de matriz es: Cuadrada, rectangular o dispersa
     *
     * @return una cadena con el tipo de matriz
     */
    public String getTipo() {
        return null;
    }
}
