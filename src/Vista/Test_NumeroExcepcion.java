/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.NumeroComplejo;
import Modelo.NumeroComplejo_Excepcion;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author madarme
 */
public class Test_NumeroExcepcion {

    public static void main(String[] args) {
        NumeroComplejo_Excepcion x = crearComplejo();
        System.out.println(x.toString());

    }

    private static NumeroComplejo_Excepcion crearComplejo() {
        NumeroComplejo_Excepcion x;
        System.out.println("-------------------- Creando Complejos ----------------\n");
        try {
            x = new NumeroComplejo_Excepcion(leerFloat("Digite parte Real:"), leerFloat("Digite parte Imaginario:"));
            return x;
        } catch (Exception ex) {
            System.out.println("Error:" + ex.getMessage());
            return crearComplejo();
        }

    }

    private static float leerFloat(String msg) {
        System.out.print(msg);
        Scanner in = new Scanner(System.in);
        try {
            return in.nextFloat();
        } catch (java.util.InputMismatchException ex) {
            System.err.println("Error no es un Float");
            return leerFloat(msg);
        }

    }

}
