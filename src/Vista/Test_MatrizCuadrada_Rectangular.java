/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Negocio.*;


import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 *
 * @author madar
 */
public class Test_MatrizCuadrada_Rectangular {

    public static void main(String[] args) throws Exception {
        int cantFilas = leerEntero("Digite la cantidad filas?:");
        int columnas = leerEntero("Digite cuántas columnas le gustaría?:");
        //-------------------------------------------------
         
        //utilizo el constructor de filas:
        Matriz_Numeros myMatriz = new Matriz_Numeros(cantFilas);
        for (int i = 0; i < myMatriz.length_filas(); i++) {
            Random r = new Random();
            //Esté número puede dar 0 
            ListaNumeros myLista_columnas = new ListaNumeros(columnas);
            for (int j = 0; j < myLista_columnas.length(); j++) {
                myLista_columnas.adicionar(j, r.nextInt(100));
            }
            //Adicionando el vector que contiene las columnas a la matriz:
            myMatriz.adicionarVector(i, myLista_columnas);

        }
        //-------------------------------------------------------
        System.out.println("Mi matriz es:\n"+myMatriz.toString());
        
        int n = leerEntero("¿Desea modificar algún elemento de la matriz? (1 - Sí) (2 - No): \n");
        while (n == 1) {
               
           try {
                int fil=leerEntero("Digite la fila del valor que desea cambiar:");
                int  col=leerEntero("Digite la columna del numero que desea cambiar:");
                float valor=leerEntero("Digite el nuevo valor que desea cambiar:");
                
                
               
                myMatriz.setElemento(fil, col, valor);
                
              
                
                
              //*****************************************************************************************  
                System.out.println("Su matriz es:\n"+myMatriz.toString());
                n = leerEntero("¿Desea modificar otro elemento de la matriz? (1 - Sí) (2 - No): \n");
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
         System.out.println("Ok");
    }

    private static int leerEntero(String msg) {
        System.out.print(msg);
        Scanner in = new Scanner(System.in);
        try {
            return in.nextInt();
        } catch (java.util.InputMismatchException ex) {
            System.err.println("Error no es un Float");
            return leerEntero(msg);
        }

    }
    
    private static int Setear(String msg){
        
       System.out.print(msg);
        Scanner in = new Scanner(System.in);
        try {
            return in.nextInt();
        } catch (java.util.InputMismatchException ex) {
            System.err.println("Error no es un Float");
            return leerEntero(msg);
        }
    }
    
}
